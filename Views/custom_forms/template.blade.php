<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-note font-green"></i>
                    <span class="caption-subject bold uppercase"> Edit Template</span>
                </div>
            </div>
            <div class="portlet-body">
                {{ Form::open(['url' => '/email_template', 'id' => 'email_template', 'class' => 'form-horizontal']) }}
                    {{ Form::hidden('id', $customForm->id) }}
                    <div class="form-body">
                        <div class="alert alert-success display-hide">
                        </div>
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> 
                            <strong>{{ trans('vbilling.error') }}</strong> {{ trans('vbilling.some_form_errors') }} 
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email Subject <span class="required" aria-required="true">* </span></label>
                                    <div class="col-md-8">
                                        {{ Form::text('email_subject', $customForm->email_subject, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email Template <span class="required" aria-required="true">* </span></label>
                                    <div class="col-md-10">
                                        {{ Form::textarea('email_template', $customForm->email_template, ['id' => 'editor', 'class' => 'form-control', 'size' => '30x6', 'data-error-container' => '#template_error']) }}
                                        <div id="template_error"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Dynamic Values <span class="required" aria-required="true">* </span></label>
                                    <div class="col-md-10">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr><td colspan="2"><a href="javascript:;" class="copy" id="all_answers">[[All Answers]]</a></td></tr>
                                                <tr>
                                                    <?php
                                                    $i = 1;
                                                    foreach ($customForm->customField as $row):?>
                                                        <td><a href="javascript:;" class="copy" id="{{ $row->id }}">[[{{ $row->question }}]]</a></td>
                                                        @if($i % 2 == 0)
                                                            </tr><tr>
                                                        @endif    
                                                    <?php
                                                        $i++;
                                                    endforeach;?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <span class="notifications"></span>
                                <?php $i = 1;?>
                                @foreach($customForm->emailNotification as $row)
                                    <div class="notifications">
                                        <div class="col-md-12">
                                            <h4 class="custom">Rule # {{ $i }}</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-1" style="margin-top: 35px">IF</div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Field <span class="required" aria-required="true">* </span></label>
                                                        <div id="custom_field{{ $i }}" class="input-icon right custom_field">
                                                            <i class="fa"></i>
                                                            {{ Form::hidden('notification_id[]', $row->id) }}
                                                            {{ getCustomFields($customForm->id, $row->custom_field_id) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Condition <span class="required" aria-required="true">* </span></label>
                                                        <div id="condition{{ $i }}" class="input-icon right condition">
                                                            <i class="fa"></i>
                                                           {{ getConditions($i, $row->conditions) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="field_values{{ $i }}" class="col-md-3 {{ ($row->conditions == 'is_empty' || $row->conditions == 'is_not_empty')?'display-hide':'' }}">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Values <span class="required" aria-required="true">* </span></label>
                                                            <div id="field_value{{ $i }}" class="input-icon right field_value">
                                                                <i class="fa"></i>
                                                                {{ Form::text('values[]', $row->values, ['id' => 'value'.$i, 'placeholder' => 'Enter Value', 'class' => 'form-control']) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-1" style="margin-top: 35px">Then</div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Email(s) <span class="required" aria-required="true">* </span></label>
                                                        <div id="emails{{ $i }}" class="input-icon right emails">
                                                            <i class="fa fa-info-circle tooltips" data-original-title="Multiple Emails Enter With Comma Separated (,)"></i>
                                                            {{ Form::textarea('email[]', $row->emails, ['class' => 'form-control', 'size' => '30x3']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <label class="control-label">&nbsp;</label>
                                                <a href="javascript:;" class="btn btn-danger delete_notification" style="margin-top: 25px">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++;?>
                                @endforeach
                                <div class="col-md-12">
                                    <hr>
                                    <a href="javascript:;" id="add_notification" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Add Notification
                                    </a>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                {{ Form::submit('Submit', ['class' => 'btn green']) }}
                                {{ Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn default']) }}
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
 
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        script.init();
        
        CKEDITOR.replace('editor');
        
//-------------------------------Add Notification-------------------------------
       
        $('#add_notification').click(function()
        {
            var i = $('div.notifications').length + 1;
            
            $.get(base_url+'/add_notification/{{ $customForm->id }}/'+i, function(data) 
            {
                $('.notifications:last').after(data);
                $(this).slideDown();
            });
            
            return false;
        });

//-----------------------------Delete Notification------------------------------

        $('.form-horizontal').on('click', '.delete_notification', function()
        {
            $(this).parent().parent().parent().remove();
            
            $('.custom_field').each(function(i) {
                $(this).attr('id', 'custom_field'+i);
            });
            $('.condition').each(function(i) {
                $(this).attr('id', 'condition'+i);
            });
            $('.field_value').each(function(i) {
                $(this).attr('id', 'field_value'+i);
            });
            $('.emails').each(function(i) {
                $(this).attr('id', 'emails'+i);
            });
            
            $(this).slideUp();
            return false;
        });


//----------------------------Show/Hide Field Values----------------------------

        $('.form-horizontal').on('change', '.conditions', function()
        {
            var id = $(this).attr('id');
            var condition = $(this).val();

            if(condition == 'is_empty' || condition == 'is_not_empty')
            {
                $('#value'+id).removeAttr('required');
                $('#field_values'+id).hide();
            }else
            {
                $('#value'+id).attr('required', 'true');
                $('#field_values'+id).show();
            }
        });
        
//-------------------------------------Copy-------------------------------------

        $('.copy').click(function()
        {
            var success = $('.alert-success');
            var element = $(this).attr('id');
            var $temp = $("<input>");
            $('body').append($temp);
            $temp.val($('#'+element).text()).select();
            document.execCommand('copy');
            $temp.remove();
            success.show();
            success.html('Copied!');
        });
        
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/custom_forms';
            return_page(url);
            return false;
        });
    });
</script>
