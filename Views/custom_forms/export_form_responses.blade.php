<?php $keys = array();?>
<table>
    <thead>
        <tr><td colspan="{{ $customForm->customField->count() }}">{{ $customForm->title }}</td></tr>
        @if($customForm->description != '')
            <tr><td colspan="{{ $customForm->customField->count() }}">{{ $customForm->description }}</td></tr>
        @endif
        <tr>
            <th>Created Date</th>
            <th>Created Time</th>
            @foreach ($customForm->customField as $row)
                <?php $keys[] = $row->id;?>
                <th>{{ $row->question }}</th>
            @endforeach 
        </tr>
    </thead>
    <tbody>
        @foreach ($customFormData as $row)
            <tr>
                <td>
                    {{ $row->created_at->format('Y-m-d') }}
                </td>
                <td>
                    {{ $row->created_at->format('H:i:s') }}
                </td>
                @foreach($keys as $key)
                    @if(isset($row->field_values[$key]))
                        <?php $fieldValue = $row->field_values[$key];?>
                        <td>
                            @if(is_array($fieldValue))
                                {{ join(', ', $fieldValue) }}
                            @else
                                {{ $fieldValue }}
                            @endif
                        </td>
                    @else
                        <td></td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
