<div class="notifications">
    <div class="col-md-12">
        <h4 class="custom">Rule # {{ $i }}</h4>
    </div>
    <div class="col-md-12">
        <div class="col-md-1" style="margin-top: 35px">IF</div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Field <span class="required" aria-required="true">* </span></label>
                    <div id="custom_field{{ $i }}" class="input-icon right custom_field">
                        <i class="fa"></i>
                        {{ getCustomFields($formId) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Condition <span class="required" aria-required="true">* </span></label>
                    <div id="condition{{ $i }}" class="input-icon right condition">
                        <i class="fa"></i>
                       {{ getConditions($i) }}
                    </div>
                </div>
            </div>
        </div>
        <div id="field_values{{ $i }}" class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Values <span class="required" aria-required="true">* </span></label>
                        <div id="field_value{{ $i }}" class="input-icon right field_value">
                            <i class="fa"></i>
                            {{ Form::text('values[]', null, ['id' => 'value'.$i, 'placeholder' => 'Enter Value', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-1" style="margin-top: 35px">Then</div>
        <div class="col-md-9">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Email(s) <span class="required" aria-required="true">* </span></label>
                    <div id="emails{{ $i }}" class="input-icon right emails">
                        <i class="fa fa-info-circle tooltips" data-original-title="Multiple Emails Enter With Comma Separated (,)"></i>
                        {{ Form::textarea('email[]', null, ['class' => 'form-control', 'size' => '30x3']) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <label class="control-label">&nbsp;</label>
            <a href="javascript:;" class="btn btn-danger delete_notification" style="margin-top: 25px">
                <i class="fa fa-close"></i>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        $('.select2').select2({
            width: null,
            minimumResultsForSearch: 7
        }).on('change', function() {
            $(this).valid();
        });
        
        $('.tooltips').tooltip()
    });
</script>
