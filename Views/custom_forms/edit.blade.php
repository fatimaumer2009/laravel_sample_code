<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-settings font-green"></i>
                    <span class="caption-subject bold uppercase"> Custom Form</span>
                </div>
            </div>
            <div class="portlet-body">
                {{ Form::model($customForm, [
                    'method' => 'POST',
                    'url' => ['/custom_forms/edit'],
                    'id' => 'custom_form', 'class' => 'form-horizontal'
                ]) }}
                    {{ Form::hidden('id', null) }}
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> 
                            <strong>{{ trans('vbilling.error') }}</strong> {{ trans('vbilling.some_form_errors') }} 
                        </div>
                        <div class="row">  
                            <div class="col-md-6">
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-4">
                                        Customers<span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <div id="customer_id" class="input-icon right">
                                            <i class="fa"></i>
                                            {{ getCustomers() }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Email(s)</label>
                                    <div class="col-md-8">
                                        <div class="input-icon right">
                                            <i class="fa fa-info-circle tooltips" data-original-title="Multiple Emails Enter With Comma Separated (,)"></i>
                                            {{ Form::textarea('emails', null, ['class' => 'form-control', 'size' => '30x3']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-4">
                                        Email
                                    </label>
                                    <div class="col-md-7">
                                        <div id="email" class="input-icon right">
                                            <i class="fa"></i>
                                            {{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        {{ Form::hidden('enable_email', 0) }}
                                        <div class="mt-checkbox-inline">
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                {{ Form::checkbox('enable_email', 1) }}
                                                <span></span>
                                            </label>                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-4">
                                        Title<span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <div id="title" class="input-icon right">
                                            <i class="fa"></i>
                                            {{ Form::text('title', null, ['placeholder' => 'Title', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Description</label>
                                    <div class="col-md-8">
                                        {{ Form::textarea('description', null, ['class' => 'form-control', 'size' => '30x3']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <span class="custom_fields"></span>
                                <?php $i = 0;?>
                                @foreach($customForm->customField as $row)
                                    <div class="custom_fields">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Question <span class="required" aria-required="true">* </span></label>
                                                    <div id="question{{ $i }}" class="input-icon right question">
                                                        <i class="fa"></i>
                                                        {{ Form::hidden('field_id[]', $row->id) }}
                                                        {{ Form::text('question[]', $row->question, ['placeholder' => 'Question', 'class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Field Type <span class="required" aria-required="true">* </span></label>
                                                    <div id="field_type{{ $i }}" class="input-icon right field_type">
                                                        <i class="fa"></i>
                                                       {{ getCustomFieldTypes($i, $row->field_type_id) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Sort Order</label>
                                                    {{ Form::text('sort_order[]', $row->sort_order, ['placeholder' => 'Sort Order', 'class' => 'form-control digits']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Required</label>
                                                    <div class="mt-checkbox-inline">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            {{ Form::checkbox('required['.$i.']', 1, $row->required) }}
                                                            {{ Form::hidden('default[]', $i) }}
                                                            <span></span>
                                                        </label>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="field_values{{ $i }}" class="col-md-4 {{ ($row->field_values == '')?'display-hide':'' }}">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Field Values <span class="required" aria-required="true">* </span></label>
                                                    <div id="field_value{{ $i }}" class="input-icon right field_value">
                                                        <i class="fa fa-info-circle tooltips" data-original-title="Must Enter Comma Separated (,) Values"></i>
                                                        {{ Form::textarea('field_values[]', $row->field_values, ['id' => 'fields'.$i, 'class' => 'form-control', 'size' => '30x3']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="control-label">&nbsp;</label>
                                            <a href="javascript:;" class="btn btn-danger delete_custom_field" style="margin-top: 25px">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <?php $i++;?>
                                @endforeach
                                <div class="col-md-12">
                                    <hr>
                                    <a href="javascript:;" id="add_custom_field" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Add Question
                                    </a>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                {{ Form::submit('Submit', ['class' => 'btn green']) }}
                                {{ Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn default']) }}
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        script.init();
        
//-------------------------------Add Custom Field-------------------------------
       
        var no = {{ $i }};
        
        $('#add_custom_field').click(function()
        {
            var i = $('div.custom_fields').length;
            
            $.get(base_url+'/add_custom_field/'+i+'/'+no, function(data) 
            {
                $('.custom_fields:last').after(data);
                $(this).slideDown();
            });
            
            no = no + 1;
            return false;
        });

//-----------------------------Delete Custom Field------------------------------

        $('.form-horizontal').on('click', '.delete_custom_field', function()
        {
            $(this).parent().parent().remove();
            
            $('.question').each(function(i) {
                $(this).attr('id', 'question'+i);
            });
            $('.field_type').each(function(i) {
                $(this).attr('id', 'field_type'+i);
            });
            $('.field_value').each(function(i) {
                $(this).attr('id', 'field_value'+i);
            });
            
            $(this).slideUp();
            return false;
        });


//----------------------------Show/Hide Field Values----------------------------

        $('.form-horizontal').on('change', '.field_type_id', function()
        {
            var id = $(this).attr('id');
            var field_type = $(this).val();

            if(field_type == 2 || field_type == 3 || field_type == 4)
            {
                $('#fields'+id).attr('required', 'true');
                $('#field_values'+id).show();
            }else
            {
                $('#fields'+id).removeAttr('required');
                $('#field_values'+id).hide();
            }
        });
        
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/custom_forms';
            return_page(url);
            return false;
        });
    });
</script>
