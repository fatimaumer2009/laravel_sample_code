<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-note font-green"></i>
                    <span class="caption-subject bold uppercase"> {{ $customForm->title }}</span>
                </div>
            </div>
            <div class="portlet-body">
                {{ Form::model($customForm, [
                    'method' => 'POST',
                    'url' => ['/form_fill'],
                    'id' => 'form_fill', 'class' => 'form-horizontal'
                ]) }}
                    {{ Form::hidden('form_id', $customForm->id) }}
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> 
                            <strong>{{ trans('vbilling.error') }}</strong> {{ trans('vbilling.some_form_errors') }} 
                        </div>
                        @if($customForm->description != '')
                            <h4 class="custom">{{ $customForm->description }}</h4>
                        @endif
                        <div class="col-md-8">
                            @if($customForm->customField->count() > 0) 
                                @foreach ($customForm->customField as $record)
                                <?php        
                                    $fieldId     = $record->id;
                                    $name        = $record->question;
                                    $fieldType   = $record->field_type_id;
                                    $fieldValues = explode(',', $record->field_values);
                                    $required    = $record->required;
                                ?>  
                                        <div class="form-group">
                                            <label class="control-label">
                                                {{ $name }}
                                                @if($required == TRUE)
                                                    <span class="required" aria-required="true">* </span>
                                                @endif
                                            </label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                @if($fieldType == 1) <!--Short Answer-->
                                                    {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control', 'required' => $required]) }}

                                                @elseif($fieldType == 2) <!--Dropdown List-->
                                                    {{ customField($fieldValues, $fieldId, $required) }}

                                                @elseif($fieldType == 3) <!--Multiple Choice-->
                                                    <div class="mt-radio-inline" style="padding-bottom: 0px">
                                                        @if($required == FALSE)
                                                            {{ Form::hidden('field_values['.$fieldId.']', null) }}
                                                        @endif
                                                        @foreach($fieldValues as $value)
                                                            <label class="mt-radio mt-radio-outline" style="margin-bottom: 0px">
                                                                {{ Form::radio('field_values['.$fieldId.']', $value, null, ['required' => $required]) }} {{ $value }}
                                                                <span></span>
                                                            </label>
                                                        @endforeach
                                                    </div>

                                                @elseif($fieldType == 4) <!--Checkbox-->
                                                    <div class="mt-checkbox-inline" style="padding-bottom: 0px">
                                                        {{ Form::hidden('field_values['.$fieldId.']', null) }}
                                                        @foreach($fieldValues as $value)
                                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px">
                                                                {{ Form::checkbox('field_values['.$fieldId.'][]', $value, null, ['required' => $required]) }} {{ $value }}
                                                                <span></span>
                                                            </label>
                                                        @endforeach
                                                    </div>

                                                @elseif($fieldType == 5) <!--Paragraph-->
                                                    {{ Form::textarea('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control', 'size' => '30x3', 'required' => $required]) }}

                                                @elseif($fieldType == 6) <!--Date-->
                                                    <div class="input-group">
                                                        {{ Form::text('field_values['.$fieldId.']', date('Y-m-d'), ['placeholder' => $record->fieldType->name, 'class' => 'form-control date-picker', 'required' => $required]) }}
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>

                                                @elseif($fieldType == 7) <!--Time-->
                                                    <div class="input-group">
                                                        {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control timepicker', 'required' => $required]) }}
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div>

                                                @elseif($fieldType == 8) <!--Email-->
                                                    {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control', 'email' => 'true', 'required' => $required]) }}

                                                @elseif($fieldType == 9) <!--Digits-->
                                                    {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control digits', 'required' => $required]) }}

                                                @elseif($fieldType == 10) <!--Number-->
                                                    {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control decimal', 'required' => $required]) }}

                                                @elseif($fieldType == 11) <!--Phone Number-->
                                                    {{ Form::text('field_values['.$fieldId.']', null, ['placeholder' => $record->fieldType->name, 'class' => 'form-control mask_phone', 'required' => $required]) }}

                                                @endif    
                                            </div> 
                                        </div>                                                        
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                {{ Form::submit('Submit', ['class' => 'btn green']) }}
                                {{ Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn default']) }}
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
//-----------------------------Load JS Functions--------------------------------
        
        script.init();
        
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/custom_forms';
            return_page(url);
            return false;
        });
    });
</script>