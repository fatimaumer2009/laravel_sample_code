<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-note font-green"></i>
                    {{ $customForm->title }}
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        @if($customForm->description != '')
                            <h4 class="custom">{{ $customForm->description }}</h4>
                        @endif
                        <div class="row">
                            @if($customFormData->count() > 0) 
                                @foreach ($customFormData as $row)
                                    @if($loop->first)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-6">Created At</label>
                                                <div class="col-md-6">
                                                    <p class="form-control-static"> 
                                                        @if ($row->created_at->diffInMonths(\Carbon\Carbon::now()) >= 1)
                                                            {{ $row->created_at }}
                                                        @else
                                                           {{ $row->created_at->diffForHumans() }}
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-6">{{ $row->customField->question }}</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"> {{ $row->field_value }} </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <ul class="pager">
                            @if($previous > 0)
                                <li class="previous">
                                    <a href="{{ url('/custom_forms/'.$customForm->id.'/'.$previous.'/show') }}" class="ajaxify"> &larr; Older </a>
                                </li>
                            @endif
                            @if($next > 0)
                                <li class="next">
                                    <a href="{{ url('/custom_forms/'.$customForm->id.'/'.$next.'/show') }}" class="ajaxify"> Newer &rarr; </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
//-----------------------------Load JS Functions--------------------------------
        
        script.init();
        
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/custom_forms';
            return_page(url);
            return false;
        });
    });
</script>