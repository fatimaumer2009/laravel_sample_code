<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-note font-green"></i>
                    <span class="caption-subject bold uppercase"> {{ $customForm->title }}</span>
                </div>
                <div class="actions">
                    <button type="button" class="btn red btn-outline export">Export</button>
                </div>
            </div>
            <div class="portlet-body">
                @if($customForm->description != '')
                    <h4 class="custom">{{ $customForm->description }}</h4>
                @endif
                <div class="form-body">
                    {{ Form::open(['url' => 'form_responses', 'method' => 'get', 'id' => 'form_responses', 'class' => 'form-horizontal']) }}
                        {{ Form::hidden('form_id', $formId) }}
                        <div class="form-group margin-top-30 margin-bottom-30">
                            <div class="col-md-5">
                                {{ getCustomFields($formId, $customFieldId, TRUE) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::text('field_value', $fieldValue, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-sm yellow filter-submit"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <table id="datatable" class="table table-striped table-bordered table-hover">
                    <thead> 
                        <tr>
                            <th>Created At</th>
                            @foreach ($customForm->customField as $row)
                                <th>{{ $row->question }}</th>
                            @endforeach 
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
//-----------------------------Load JS Functions--------------------------------
        
        script.init();
        
        var table = $('#datatable');

        table.DataTable({
            'processing': true,
            'serverSide': true,
            'pageLength': 10,
            'pagingType': 'bootstrap_extended',
            'stateSave': true,
            'bFilter': false,
            'ajax': base_url+'/responses/get_data?form_id={{ $formId }}&field_value={{ $fieldValue }}&custom_field_id[]=<?php echo join('&custom_field_id[]=', $customFieldId);?>',
            'order': [
                [0, 'desc']
            ],
            'columns': [
                { data: 'created_at' },
                @foreach ($customForm->customField as $row)
                    { data: 'field_values.{{ $row->id }}', name: 'field_values->{{ $row->id }}' },    
                @endforeach 
            ]
        });
        
        
//------------------------------------Lookup------------------------------------

        $('#form_responses').submit(function()
        {
            lookup($(this));
            return false;        
        });  
        
//------------------------------------Export------------------------------------
        
        $('.export').click(function()
        {
            window.location = base_url +'/export_form_responses/{{ $formId }}';
        });
        
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/custom_forms';
            return_page(url);
            return false;
        });
    });
</script>