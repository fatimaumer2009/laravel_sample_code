<?php $authentication = \App::make('authentication_helper'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-settings font-green"></i>
                    <span class="caption-subject bold uppercase"> Custom Forms</span>
                </div>
            </div>
            <div class="portlet-body">
                @if($authentication->hasPermission(['_superadmin', '_custom-form-create']))
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ url('/custom_forms/edit') }}" class="btn sbold green ajaxify"> 
                                        Add New <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                                <div id="action_btn" class="btn-group" style="display: none">
                                    <a href="javascript:;" id="delete_multiple" class="btn sbold green"> 
                                        Delete <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('flash_message'))
                    <div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>{{ trans('vbilling.success') }}</strong>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="note note-info display-hide" style="text-align: center;">
                    <h5></h5>
                </div>
                {{ Form::hidden('total_records', null, ['id' => 'total_records']) }}
                {{ Form::hidden('all_records', 0, ['id' => 'all_records']) }}
                <table id="datatable" class="table table-striped table-bordered table-hover table-checkable order-column">
                    <thead> 
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#datatable .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Customer</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customForms as $row)
                            <?php $response = App\CustomFormData::where('form_id', $row->id)->count();?>
                            <tr>
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{ $row->id }}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->description }}</td>
                                <td>{{ getCustomerName($row) }}</td>
                                <td>{{ $row->created_at }}</td>
                                <td>
                                    @if($authentication->hasPermission(['_superadmin', '_custom-form-edit']))
                                        <a href="{{ url('/custom_forms/edit?id='.$row->id) }}" class="ajaxify" title="Edit Form"><i class="fa fa-edit"></i></a>
                                    @endif
                                    @if($authentication->hasPermission(['_superadmin', '_fill-form']))
                                        <a href="{{ url('/form_fill/'.$row->id) }}" class="ajaxify" title="Fill Form"><i class="fa fa-pencil"></i></a>
                                    @endif
                                    @if($authentication->hasPermission(['_superadmin', '_template-edit']))
                                        <a href="{{ url('/email_template/'.$row->id) }}" class="ajaxify" title="Email Template"><i class="fa fa-envelope"></i></a>
                                    @endif
                                    @if($authentication->hasPermission(['_superadmin', '_custom-form-create']))
                                        <a href="javascript:;" id="{{ $row->id }}" class="duplicate_form" title="Duplicate Form"><i class="fa fa-clone"></i></a>
                                    @endif
                                    @if($authentication->hasPermission(['_superadmin', '_custom-form-delete']))
                                        <a href="javascript:;" id="{{ $row->id }}" class="delete" title="Delete Form"><i class="fa fa-trash-o"></i></a>
                                    @endif
                                    @if($response > 0)
                                        @if($authentication->hasPermission(['_superadmin', '_form-responses-view']))
                                            <a href="{{ url('/form_responses?form_id='.$row->id) }}" class="ajaxify" title="Responses"><i class="fa fa-folder-o"></i></a>
                                        @endif
                                    @endif
                                </td> 
                            </tr>
                        @endforeach                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="duplicate_form_modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Duplicate Form</h4>
            </div>
            <div class="modal-body">  
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button> 
                    <strong>{{ trans('vbilling.error') }}</strong> {{ trans('vbilling.some_form_errors') }} 
                </div>                
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Title<span class="required"> * </span>
                        </label>
                        <div class="col-md-7">
                            {{ Form::hidden('form_id', null, ['id' => 'form_id']) }}
                            {{ Form::text('title', null, ['id' => 'title', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">{{ trans('vbilling.close') }}</button>
                {{ Form::submit(trans('vbilling.submit'), ['id' => 'duplicate_form', 'class' => 'btn green']) }}                                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
//-----------------------------Load JS Functions--------------------------------
        
        script.init();
        
        var return_url = base_url+'/custom_forms';
        
//------------------------------------Table-------------------------------------
        
        var table = $('#datatable');

        table.dataTable({
            'bStateSave': true, // save datatable state(pagination, sort, etc) in cookie.
            'pagingType': 'bootstrap_extended',            
            "paging": true,
            'pageLength': 25,
            'columnDefs': [{  // set default column settings
                'orderable': false,
                'targets': [0, -1]
            }, {
                'searchable': false,
                'targets': [0, -1]
            }],
            'fnDrawCallback': function () {
                var total = this.fnSettings().fnRecordsTotal();
                $('#total_records').val(total);
            }
        });
        
//--------------------------------Delete Record---------------------------------

        $('.delete').click(function()
        {    
            var id = $(this).attr('id');
            var url = base_url +'/custom_forms/'+id+'/destroy';
            delete_record(url, return_url);
        });

//---------------------------Delete Multiple Records----------------------------

        $('#delete_multiple').click(function()
        {   
            var data;
            var form_ids = [];
            $('.checkboxes:checked').each(function() 
            {
                form_ids.push($(this).val());
            });
            
            if($('#all_records').val() == 1)
            {
                data = 'all=1';
            }else
            {
                data = 'form_ids='+form_ids;
            }
            
            data = data+'&_token={{ csrf_token() }}';
            
            var url = base_url +'/custom_forms/delete_multiple_records';
            
            bootbox.confirm("{{ trans('vbilling.delete_record_confirmation') }}", function(result) 
            {
                if(result == true)
                {
                    ajax_call2(data, url, return_url);
                }
            });
        });
        
//-------------------------Checked/Unchecked Record(s)--------------------------

        table.find('.group-checkable').change(function () 
        {
            var set = jQuery(this).attr('data-set');
            var checked = jQuery(this).is(':checked');
            jQuery(set).each(function () 
            {
                if (checked) 
                {
                    $(this).prop('checked', true);
                    $(this).parents('tr').addClass('active');

                } else 
                {
                    $(this).prop('checked', false);
                    $(this).parents('tr').removeClass('active');
                }
            });

            if (checked) 
            {
                var total   = $('#total_records').val();
                var display = $('.checkboxes:checked').length;
                
                if(display > 0)
                {
                    $('#action_btn').show();
                }else
                {
                    $('#action_btn').hide();
                }
                
                if(total > display)
                {
                    $('.note h5').html("{{ trans('vbilling.all') }} "+display+" {{ trans('vbilling.messages_on_this_page_are_selected') }} <a href='javascript:;' id='select_all'> {{ trans('vbilling.select_all') }} "+total+" {{ trans('vbilling.messages') }}</a>");
                    $('.note').show();
                }
            }else
            {
                $('#all_records').val(0);
                $('.note').hide();
                $('#action_btn').hide();
            }
        });

        table.on('change', 'tbody tr .checkboxes', function () 
        {
            var count = $('.checkboxes:checked').length;
            $(this).parents('tr').toggleClass('active');           
            $('#all_records').val(0);
            $('.note').hide();
            
            if(count > 0)
            {
                $('#action_btn').show();
            }else
            {
                $('#action_btn').hide();
            }
        });
        
//-----------------------------Select All Record(s)-----------------------------

        $('.note').on('click', '#select_all', function ()
        {
            $('#all_records').val(1);
            
            var total = $('#total_records').val();
                    
            $('.note h5').html("{{ trans('vbilling.all') }} "+total+" {{ trans('vbilling.messages_are_selected') }} <a href='javascript:;' id='clear_all'>{{ trans('vbilling.clear_selection') }}</a>");
        });

//-----------------------------Clear All Record(s)------------------------------

        $('.note').on('click', '#clear_all', function ()
        {
            $('#all_records').val(0);
            $('.note').hide();
            $('.group-checkable').prop("checked", false);
            
            $('.checkboxes').each(function() 
            {
                $(this).prop("checked", false);
                $(this).parents('tr').removeClass("active");
                    
            });
        });
        
//-----------------------------Duplicate Form Modal-----------------------------

        $('.duplicate_form').click(function()
        {
            var id = $(this).attr('id');      
            $('#form_id').val(id);
            $('#duplicate_form_modal').modal('show');
        });

//--------------------------------Duplicate Form--------------------------------

        $('#duplicate_form').click(function()
        {   
            var error = $('.alert-danger')
            var id = $('#form_id').val();
            var title = $('#title').val();
            var data = 'id='+id+'&title='+title+'&_token={{ csrf_token() }}';
            var url = base_url +'/duplicate_form';
            
            if(title == '')
            {
                error.show();
                error.html('Title Field is required!');
            }else
            {
                error.hide();
                ajax_call2(data, url, return_url);
            }
            return false;
        });
    });
</script>
