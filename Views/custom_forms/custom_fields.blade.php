<div class="custom_fields">
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Question <span class="required" aria-required="true">* </span></label>
                <div id="question{{ $i }}" class="input-icon right question">
                    <i class="fa"></i>
                    {{ Form::text('question[]', null, ['placeholder' => 'Question', 'class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Field Type <span class="required" aria-required="true">* </span></label>
                <div id="field_type{{ $i }}" class="input-icon right field_type">
                    <i class="fa"></i>
                   {{ getCustomFieldTypes($i) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Sort Order</label>
                {{ Form::text('sort_order[]', 0, ['placeholder' => 'Sort Order', 'class' => 'form-control digits']) }}
            </div>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Required</label>
                <div class="mt-checkbox-inline">
                    <label class="mt-checkbox mt-checkbox-outline">
                        {{ Form::checkbox('required['.$no.']', 1) }}
                        {{ Form::text('default[]', $no) }}
                        <span></span>
                    </label>                                                    
                </div>
            </div>
        </div>
    </div>
    <div id="field_values{{ $i }}" class="col-md-4 display-hide">
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Field Values <span class="required" aria-required="true">* </span></label>
                <div id="field_value{{ $i }}" class="input-icon right field_value">
                    <i class="fa fa-info-circle tooltips" data-original-title="Must Enter Comma Separated (,) Values"></i>
                    {{ Form::textarea('field_values[]', null, ['id' => 'fields'.$i, 'class' => 'form-control', 'size' => '30x3']) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1">
        <label class="control-label">&nbsp;</label>
        <a href="javascript:;" class="btn btn-danger delete_custom_field" style="margin-top: 25px">
            <i class="fa fa-close"></i>
        </a>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        $(".digits").TouchSpin({
            min: 0,
            max: 1000000,
            step: 1,
            boostat: 5,
            maxboostedstep: 10
        });
        
        $('.select2').select2({
            width: null,
            minimumResultsForSearch: 7
        }).on('change', function() {
            $(this).valid();
        });
        
        $('.tooltips').tooltip()
    });
</script>
