<?php

//----------------------------Get Custom Field Types----------------------------

function getCustomFieldTypes($i, $id = null)
{
    return Form::select('field_type_id[]', 
            ['' => 'Select One'] +
            App\CustomFieldType::pluck('name', 'id')->all(),
            $id, [ 
            'id' => $i, 'class' => 'form-control select2 field_type_id'
        ]);
}

//-------------------------------Get Form Fields--------------------------------

function getCustomFields($formId, $id = null, $multiple = FALSE)
{
    return Form::select('custom_field_id[]', 
            App\CustomField::where('form_id', $formId)->pluck('question', 'id')->all(),
            $id, [ 
            'class' => 'form-control select2', 'multiple' => $multiple, 'data-placeholder' => 'Select Field'
        ]);
}

//--------------------------------Get Conditions--------------------------------

function getConditions($i, $id = null)
{
    $data = array(
        ''     => 'Select Condition', 
        'Text' => array(
            'equals'           => 'Equals',
            'contains'         => 'Contains',
            'does_not_contain' => 'Does not contain',
            'is_empty'         => 'Is Empty',
            'is_not_empty'     => 'Is Not Empty'
        ),
        'Number' => array(
            'equal_to'     => 'Equal to',
            'not_equal_to' => 'Not equal to',
            'greater_than' => 'Greater than',
            'less_than'    => 'Less than'
        ),
        'Regular Expression' => array(
            'regex_matches'     => 'Matches',
            'regex_not_matches' => 'Does not match'
        )
    );
    
    return Form::select('conditions[]', 
            $data,
            $id, [ 
            'id' => $i, 'class' => 'form-control select2 conditions'
        ]);
}

//---------------------------------Custom Field---------------------------------

function customField($fieldValues, $fieldId, $required)
{
    $data = array();
    
    foreach($fieldValues as $fieldValue)
    {
        $data[$fieldValue] = $fieldValue;
    }
        
    return Form::select('field_values['.$fieldId.']', 
            ['' => 'Select One'] +
            $data,
            null, [ 
            'class' => 'form-control select2', 'required' => $required
        ]);
}

//--------------------------------Get Customers---------------------------------

function getCustomers($id = null)
{
    $data = array();
    
    $users = App\User::getCustomers();
    
    if($users->count() > 0)
    {
        foreach($users as $row)
        {
            $data[$row->id] = $row->first_name.' '.$row->last_name;
        }
    }
    
    return Form::select('customer_id', 
            ['' => 'Select Customer'] +
            $data,
            $id, [ 
            'class' => 'form-control select2'
        ]);
}

//------------------------------Get Customer Name-------------------------------

function getCustomerName($row)
{
    $firstName = $row->userProfile->first_name;
    $LastName = $row->userProfile->last_name;
    
    return ($firstName != '' || $LastName != '')?$firstName.' '.$LastName:'Customer';
}

//--------------------------------Get User Group--------------------------------

function userGroup()
{
    $auth = \App::make('authenticator');
    $loggedUser = $auth->getLoggedUser();
    $group = $loggedUser->getGroups()->first();
    return $group->name;
}

//---------------------------------Get User ID----------------------------------

function userId()
{
    $auth = \App::make('authenticator');
    $loggedUser = $auth->getLoggedUser();
    return $loggedUser->id;
}

//--------------------------------Get User Email--------------------------------

function userEmail()
{
    $auth = \App::make('authenticator');
    $loggedUser = $auth->getLoggedUser();
    return $loggedUser->email;
}

//--------------------------------Get User Name---------------------------------

function userName()
{
    $auth = \App::make('authenticator');
    $loggedUser = $auth->getLoggedUser(); 
    $profile = $loggedUser->user_profile()->first();
    
    return $profile->first_name.' '.$profile->last_name;
}

//-------------------------------Get Global Value-------------------------------

function getGlobalValue($configKey)
{
    $vConfig = \App\VConfig::where(array('config_key' => $configKey))->firstOrFail();
    return $vConfig->config_value;
}