<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['web']], function () 
{
    Route::group(['middleware' => ['admin_logged', 'can_see']], function ()
    {

/***********************************Template***********************************/

        Route::get('/', function () {
            return view('layouts.app');
        });
        
        Route::get('export_form_responses/{form_id}', [
            'middleware' => 'has_perm:_superadmin,_form-responses-view',
            'uses' => 'CustomFormsController@exportFormResponses'
        ]);
            
        Route::group(['middleware' => ['ajax']], function ()
        {

/*****************************Custom Forms Module*****************************/

            Route::get('custom_forms', [
                'middleware' => 'has_perm:_superadmin,_custom-form-view',
                'uses' => 'CustomFormsController@index'
            ]);
            
            Route::get('custom_forms/edit', [
                'middleware' => 'has_perm:_superadmin,_custom-form-create,_custom-form-edit',
                'uses' => 'CustomFormsController@edit'
            ]);

            Route::post('custom_forms/edit', [
                'middleware' => 'has_perm:_superadmin,_custom-form-create,_custom-form-edit',
                'uses' => 'CustomFormsController@update'
            ]);
            
            Route::get('custom_forms/{id}/destroy', [
                'middleware' => 'has_perm:_superadmin,_custom-form-delete',
                'uses' => 'CustomFormsController@destroy'
            ]);
            
            Route::post('custom_forms/delete_multiple_records', [
                'middleware' => 'has_perm:_superadmin,_custom-form-delete',
                'uses' => 'CustomFormsController@deleteMultipleRecords'
            ]);
            
            Route::get('email_template/{id}', [
                'middleware' => 'has_perm:_superadmin,_template-edit',
                'uses' => 'CustomFormsController@editTemplate'
            ]);
            
            Route::post('email_template', [
                'middleware' => 'has_perm:_superadmin,_template-edit',
                'uses' => 'CustomFormsController@updateTemplate'
            ]);
            
            Route::get('form_fill/{id}', [
                'middleware' => 'has_perm:_superadmin,_fill-form',
                'uses' => 'CustomFormsController@formFill'
            ]);
            
            Route::post('form_fill', [
                'middleware' => 'has_perm:_superadmin,_fill-form',
                'uses' => 'CustomFormsController@storeFormFill'
            ]);
            
            Route::post('duplicate_form', [
                'middleware' => 'has_perm:_superadmin,_custom-form-create',
                'uses' => 'CustomFormsController@duplicateForm'
            ]);
            
            Route::get('form_responses', [
                'middleware' => 'has_perm:_superadmin,_form-responses-view',
                'uses' => 'CustomFormsController@formResponses'
            ]);
            
            Route::get('responses/get_data', [
                'middleware' => 'has_perm:_superadmin,_form-responses-view',
                'uses' => 'CustomFormsController@formResponsesData'
            ]);
            
            Route::get('users/create', [
                'middleware' => 'has_perm:_superadmin,_user-editor',
                'uses' => 'CustomFormsController@createUser'
            ]);

            Route::post('users', [
                'middleware' => 'has_perm:_superadmin,_user-editor',
                'uses' => 'CustomFormsController@storeUser'
            ]);

            Route::get('add_custom_field/{i}/{no}', [
                'middleware' => 'has_perm:_superadmin,_custom-form-create,_custom-form-edit',
                'uses' => 'CustomFormsController@addCustomField'
            ]);
            
            Route::get('add_notification/{form_id}/{i}', [
                'middleware' => 'has_perm:_superadmin,_template-edit',
                'uses' => 'CustomFormsController@addNotification'
            ]);
            
/*******************************vConfigs Module********************************/
        
            Route::get('v_configs', [
                'middleware' => 'has_perm:_superadmin,_v-config-view',
                'uses' => 'VConfigsController@index'
            ]);

            Route::get('v_configs/create', [
                'middleware' => 'has_perm:_superadmin,_v-config-create',
                'uses' => 'VConfigsController@create'
            ]);

            Route::post('v_configs', [
                'middleware' => 'has_perm:_superadmin,_v-config-create',
                'uses' => 'VConfigsController@store'
            ]);

            Route::get('v_configs/{id}/edit', [
                'middleware' => 'has_perm:_superadmin,_v-config-edit',
                'uses' => 'VConfigsController@edit'
            ]);

            Route::patch('v_configs/{id}', [
                'middleware' => 'has_perm:_superadmin,_v-config-edit',
                'uses' => 'VConfigsController@update'
            ]);

            Route::get('v_configs/{id}/destroy', [
                'middleware' => 'has_perm:_superadmin,_v-config-delete',
                'uses' => 'VConfigsController@destroy'
            ]);
        });
    });
});