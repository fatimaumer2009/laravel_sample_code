var script = function () {
    
    var customForm = function() {
        
        var return_url = base_url+'/custom_forms';
        var form       = $('#custom_form');
        var error      = $('.alert-danger', form);
        
        form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  

            rules: {
                customer_id: {
                    required: true
                },
                title: {
                    required: true
                },
                email: {
                    email: true
                },
                'question[]': {
                    required: true
                },
                'field_type_id[]': {
                    required: true
                }
            },
            
            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
                $(element)
                    .closest('.form-group').find('.select2-selection__arrow').css('right', '30px');
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call($(form), return_url);
            }
        });
    };
    
    var formFill = function() {
        
        var return_url = base_url+'/custom_forms';
        var form       = $('#form_fill');
        var error      = $('.alert-danger', form);
        
        form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  

            rules: {
                form_id: {
                    required: true
                }
            },
            
            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                if(element.closest('.mt-checkbox-outline').length > 0)
                {
                    error.insertAfter(element.closest('.mt-checkbox-inline'));
                }else if(element.closest('.mt-radio-outline').length > 0)
                {
                    error.insertAfter(element.closest('.mt-radio-inline'));
                }else
                {    
                    var icon = $(element).closest('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass('fa-warning');  
                    icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
                    $(element)
                        .closest('.form-group').find('.select2-selection__arrow').css('right', '30px');
                    if(element.closest('.input-group').length > 0)
                    {
                        icon.css('right', '42px');
                    }
                }
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                if($(element).closest('.mt-checkbox-outline').length > 0)
                {}else if($(element).closest('.mt-radio-outline').length > 0)
                {}else
                {               
                    var icon = $(element).closest('.input-icon').children('i');
                    icon.removeClass('fa-warning').addClass('fa-check');
                }
                
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call($(form), return_url);
            }
        });
    };
    
    var emailTemplate = function() {

        var return_url = base_url+'/custom_forms';
        var form       = $('#email_template');
        var error      = $('.alert-danger', form);
        
        form.on('submit', function() {
            for(var instanceName in CKEDITOR.instances) {
                CKEDITOR.instances[instanceName].updateElement();
            }
        });

        form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  

            rules: {
                email_subject: {
                    required: true
                },
                email_template: {
                    required: true
                },
                'custom_field_id[]': {
                    required: true
                },
                'conditions[]': {
                    required: true
                },
                'emails[]': {
                    required: true
                }
            },
            
            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
                $(element)
                    .closest('.form-group').find('.select2-selection__arrow').css('right', '30px');
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call($(form), return_url);
            }
        });
    };
    
    var vConfig = function() {
        
	var return_url = base_url +'/v_configs';
        var form       = $('#v_config');
        var error      = $('.alert-danger', form);
       
        form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  
            rules: {
                config_key: {
                    required: true,
                    alphanumeric: true,
                    maxlength: 30
                },
                config_value: {
                    required: true,
                    maxlength: 255
                },
                possible_values: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call($(form), return_url);
            }
	});
    };

    var addUser = function() {
        
	var form  = $('#add_user');
	var error = $('.alert-danger', form);
        var return_url = base_url +'/admin/users/list';
        
	form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    minlength: 6,
                    equalTo: '#password'
                },
                first_name: {
                    required: true,
                    maxlength: 50
                },
                last_name: {
                    required: true,
                    maxlength: 50
                },
                phone : {
                    required: true,
                    maxlength: 20
                },
                state : {
                    maxlength: 20
                },   
                vat: {
                    maxlength: 20
                },
                city: {
                    maxlength: 50
                },
                country: {
                    maxlength: 50
                },
                zip: {
                    maxlength: 20
                },
                address: {
                    maxlength: 100
                }
            },

            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call($(form), return_url);
            }
	});
    };
    
    var editUser = function() {
        
	var form  = $('#edit_user');
	var error = $('.alert-danger', form);
        
	form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    minlength: 6,
                    equalTo: '#password'
                }
            },

            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                ajax_call_acl($(form));
            }
	});
    };
    
    var userProfile = function() {
        
	var form  = $('#update_profile');
        var error = $('.alert-danger', form);
        
	form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  
            rules: {
                first_name: {
                    required: true,
                    maxlength: 50
                },
                last_name: {
                    required: true,
                    maxlength: 50
                },
                phone : {
                    required: true,
                    maxlength: 20
                },
                state : {
                    maxlength: 20
                },   
                vat: {
                    maxlength: 20
                },
                city: {
                    maxlength: 50
                },
                country: {
                    maxlength: 50
                },
                zip: {
                    maxlength: 20
                },
                address: {
                    maxlength: 100
                }                    
            },
            
            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                ajax_call_acl2($(form)); 
            }
	});
    };
    
    var updatePassword = function() {
        
	var form  = $('#update_password');
	var error = $('.alert-danger', form);
        
	form.validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false, 
            ignore: '',  
            rules: {
                password: {
                    minlength: 6
                },
                password_confirmation: {
                    minlength: 6,
                    equalTo: '#password'
                }
            },

            invalidHandler: function (event, validator) {               
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass('fa-warning');  
                icon.attr('data-original-title', error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                icon.removeClass('fa-warning').addClass('fa-check');
            },

            submitHandler: function (form) {
                error.hide();
                ajax_call_acl2($(form));
            }
	});
    };
    
    var table = function() {
        
        var table = $('.datatable');

        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            'language': {
                'aria': {
                    'sortAscending': ': activate to sort column ascending',
                    'sortDescending': ': activate to sort column descending'
                },
                'emptyTable': 'No data available in table',
                'info': 'Showing _START_ to _END_ of _TOTAL_ records',
                'infoEmpty': 'No records found',
                'infoFiltered': '(filtered1 from _MAX_ total records)',
                'lengthMenu': 'Show _MENU_',
                'search': 'Search:',
                'zeroRecords': 'No matching records found',
                'paginate': {
                    'previous':'Prev',
                    'next': 'Next',
                    'last': 'Last',
                    'first': 'First'
                }
            },
            // Uncomment below line('dom' parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //'dom': '<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>',

            'bStateSave': true, // save datatable state(pagination, sort, etc) in cookie.
            'pagingType': 'bootstrap_extended',            
            "paging": false,
/*
            'lengthMenu': [
                [5, 15, 20, -1],
                [5, 15, 20, 'All'] // change per page values here
            ],*/
            // set the initial value
            'pageLength': 25,
            'columnDefs': [{  // set default column settings
                'orderable': false,
                'targets': [-1]
            }, {
                'searchable': false,
                'targets': [-1]
            }]
        });
    };
    
    var scripts = function() {
   
//-----------------------------------Select2------------------------------------
        
        $.fn.select2.defaults.set('theme', 'bootstrap');

        $('.select2').select2({
            width: null,
            minimumResultsForSearch: 7
        }).on('change', function() {
            $(this).valid();
        });

//---------------------------------Date Picker----------------------------------

        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',                
            rtl: App.isRTL(),
            orientation: 'left',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function() {
            $(this).valid();
        });
        
//---------------------------------Time Picker----------------------------------

        $('.timepicker').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: false,
            showMeridian: false
        });
        
//-------------------------------DateTime Picker--------------------------------
/*
        $('.datetime').datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            pickerPosition: 'bottom-left'
        }).on('changeDate', function() {
            $(this).valid();
        });
*/
//----------------------------------Touchspin-----------------------------------
        
        $(".digits").TouchSpin({
            min: 0,
            max: 1000000,
            step: 1,
            boostat: 5,
            maxboostedstep: 10
        });
        
        $('.decimal').TouchSpin({
            min: 0,
            max: 1000000000000,
            step: 0.01,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10
        });
        
//-------------------------------Phone Input Mask-------------------------------
        
        $('.mask_phone').inputmask('mask', {
            'mask': '999-999-9999'
        });
        
//------------------------------Validation Methods------------------------------

        $.validator.addMethod('alphanumericdashspace', function(value, element) {
                return this.optional(element) || /^[\a-zA-Z][\a-zA-Z0-9-_\s]+$/i.test(value);
        }, alphanumericdashspace);

        $.validator.addMethod('alphanumericdash', function(value, element) {
                return this.optional(element) || /^[\a-zA-Z][\a-zA-Z0-9-]+$/i.test(value);
        }, alphanumericdash);
        
        $.validator.addMethod('numerichash', function(value, element) {
                return this.optional(element) || /^[\d#]+$/.test( value );
        }, numerichash);                    
    };
    
    return {
 
        init: function () {    
            customForm();
            formFill();
            emailTemplate();
            vConfig();
            addUser();
            editUser();
            userProfile();
            updatePassword();
            table();
            scripts();
        }
    };
}();

function lookup(val)
{
    App.scrollTop();
    var pageContentBody = $('.page-content .page-content-body');
    App.startPageLoading();

    $.ajax({
        url  : val.attr('action'),
        type : val.attr('method'),
        data : val.serialize(),
        success: function(res){
            App.stopPageLoading();
            pageContentBody.html(res);
            Layout.fixContentHeight(); 
            App.initAjax(); 
        },
        statusCode: {
            500:function() {
                pageContentBody.html(error_500);
            },
            401:function() { 
                pageContentBody.html(error_401); 
            },
            404:function() { 
                pageContentBody.html(error_404);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            App.stopPageLoading();
            pageContentBody.html(error_500);
        }
    });
}

function ajax_call(form, return_url)  
{
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);
    
    var btn = form.find($('input[type=submit]'));
    btn.attr('data-loading-text', loading);
    btn.button('loading');
    
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'json',
        success: function(data)
        {
            if(data.response == 'success')
            {
                $('.modal-backdrop').fadeOut();
                return_page(return_url);
            }else
            {
                success.hide();
                error.show();
                error.html(data.message);
                App.scrollTo(error, -200);
            }
            btn.button('reset');
        },
        error : function(data) 
        { 
            success.hide();
            error.show();
            App.scrollTo(error, -200);
                
            var status = data.status;
            if(status == 401)
            {
                error.html('<strong>'+unauthorized_error+'</strong> '+error_401_text);
            }else if(status == 404)
            {
                error.html('<strong>'+you_are_lost+'</strong> '+error_404_text);
            }else if(status == 422)
            {
                var errors = $.parseJSON(data.responseText);

                $('.form-group').each(function() {
                    $(this).removeClass('has-error').addClass('has-success'); 
                    $(this).find('i').removeClass('fa-warning').addClass('fa-check');
                });

                $.each(errors, function(index, value) {
                    var new_index = index.replace('.', ''); 
                    var icon = $('#'+new_index).children('i');
                    icon.removeClass('fa-check').addClass('fa-warning');  
                    icon.attr('data-original-title', value).tooltip({'container': 'body'});
                    icon.attr('data-original-title', value).tooltip({'container': '.modal-body'});
                    $('#'+new_index).closest('.form-group').removeClass('has-success').addClass('has-error');
                });
            }else
            {
                error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
            }
            
            btn.button('reset');
        }
    });
    return false;
}

function ajax_call2(data, url, return_url)
{
    var error   = $('.alert-danger');
    var success = $('.alert-success');
        
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'json',
        success: function(data)
        {
            if(data.response == 'success')
            {
                $('.modal-backdrop').fadeOut();
                return_page(return_url);
            }else
            {
                success.hide();
                error.show();
                $('.alert-danger span').html(data.message);
                App.scrollTo(error, -200);
            }
        },
        error : function(data) 
        {  
            success.hide();
            error.show();
            App.scrollTo(error, -200);
                
            var status = data.status;
            if(status == 401)
            {
                error.html('<strong>'+unauthorized_error+'</strong> '+error_401_text);
            }else if(status == 404)
            {
                error.html('<strong>'+you_are_lost+'</strong> '+error_404_text);
            }else
            {
                error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
            }
        }
    });
}

function ajax_call_acl(form)  
{
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);
    
    App.scrollTop();
    var pageContentBody = $('.page-content .page-content-body');
    App.startPageLoading();
    
    var btn = form.find($('input[type=submit]'));
    btn.attr('data-loading-text', loading);
    btn.button('loading');
    
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'html',
        success: function(data)
        {
            App.stopPageLoading();
            pageContentBody.html(data);
            Layout.fixContentHeight(); 
            App.initAjax();
        },
        error : function() 
        { 
            success.hide();
            error.show();
            App.scrollTo(error, -200);
            error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
            btn.button('reset');
        }
    });
    return false;
}

function ajax_call_acl2(form)  
{
    var error   = $('.alert-danger');
    var success = $('.alert-success');
    
    var btn = form.find($('input[type=submit]'));
    btn.attr('data-loading-text', loading);
    btn.button('loading');
    
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'html',
        async: false,
        success: function()
        {
            error.hide();
            success.show();
            App.scrollTo(error, -200);
            btn.button('reset');
        },
        error : function() 
        { 
            success.hide();
            error.show();
            App.scrollTo(error, -200);
            error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
            btn.button('reset');
        }
    });
    return false;
}

function ajax_call_file_upload_acl(form)  
{
    var error    = $('.alert-danger', form);
    var success  = $('.alert-success', form);
    var formData = new FormData(form[0]);
    
    App.scrollTop();
    var pageContentBody = $('.page-content .page-content-body');
    App.startPageLoading();
    
    var btn = form.find($('input[type=submit]'));
    btn.attr('data-loading-text', loading);
    btn.button('loading');
    
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: formData,
        dataType: 'html',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data)
        {
            App.stopPageLoading();
            pageContentBody.html(data);
            Layout.fixContentHeight(); 
            App.initAjax();
        },
        error : function() 
        { 
            success.hide();
            error.show();
            App.scrollTo(error, -200);
            error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
            btn.button('reset');
        }
    });
    return false;
}

function delete_record(url, return_url)
{
    var error   = $('.alert-danger');
    var success = $('.alert-success');
    bootbox.confirm(delete_record_confirmation, function(result) 
    {
        if(result==true)
        {
           $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: function(data)
                {
                    if(data.response == 'success')
                    {
                        return_page(return_url);
                    }else
                    {
                        success.hide();
                        error.show();
                        $('.alert-danger span').html(data.message);
                        App.scrollTo(error, -200);
                    }
                },
                error : function(data) {  
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);

                    var status = data.status;
                    if(status == 401)
                    {
                        error.html('<strong>'+unauthorized_error+'</strong> '+error_401_text);
                    }else if(status == 404)
                    {
                        error.html('<strong>'+you_are_lost+'</strong> '+error_404_text);
                    }else
                    {
                        error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
                    }
                }
            });
        }
    });
    return false;
}

function delete_record_acl(url)
{
    var error   = $('.alert-danger');
    var success = $('.alert-success');
    
    bootbox.confirm(delete_record_confirmation, function(result) 
    {
        if(result == true)
        {
            App.scrollTop();
            var pageContentBody = $('.page-content .page-content-body');
            App.startPageLoading();
    
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data)
                {
                    App.stopPageLoading();
                    pageContentBody.html(data);
                    Layout.fixContentHeight(); 
                    App.initAjax();
                },
                error : function() 
                { 
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                    error.html('<strong>'+something_wrong+'</strong> '+error_500_text);
                }
            });
        }
    });
    return false;
}

function return_page(url)
{
    var resBreakpointMd = App.getResponsiveBreakpoint('md');
    App.scrollTop();

    var pageContentBody = $('.page-content .page-content-body');

    App.startPageLoading();

    if (App.getViewPort().width < resBreakpointMd && $('.page-sidebar').hasClass('in')) { // close the menu on mobile view while laoding a page 
        $('.page-header .responsive-toggler').click();
    }

    $.ajax({
        type: 'GET',
        cache: false,
        url: url,
        dataType: 'html',
        success: function (res) {
            App.stopPageLoading();
            pageContentBody.html(res);
            Layout.fixContentHeight(); // fix content height
            App.initAjax(); // initialize core stuff
        },
        statusCode: {
            500:function() {
                pageContentBody.html(error_500);
            },
            401:function() { 
                pageContentBody.html(error_401); 
            },
            404:function() { 
                pageContentBody.html(error_404);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            App.stopPageLoading();
            pageContentBody.html('<h4>'+error_500+'</h4>');
        }
    });
}       