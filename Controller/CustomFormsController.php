<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use App\CustomForm;
use App\CustomField;
use App\CustomFormData;
use App\EmailNotification;
use LaravelAcl\Authentication\Models\User;
use LaravelAcl\Authentication\Models\UserProfile;
use DataTables, Excel;

class CustomFormsController extends Controller
{
    public function index(Request $request)
    {
        $customerId = $request['customer_id'];
        
        if($customerId != '')
        {
            $customForms = CustomForm::where('customer_id', $customerId)->get();
        }else if(userGroup() == 'customer')
        {
            $customForms = CustomForm::where('customer_id', userId())->get();
        }else {
            $customForms = CustomForm::get();
        }
        return view('custom_forms.index', compact('customForms'));
    }

    public function edit(Request $request)
    {
        $id = $request['id'];
        
        if($id != '')
        {
            $customForm = CustomForm::findOrFail($id);
        }else
        {
            $customForm = new CustomForm();
        }

        return view('custom_forms.edit', compact('customForm'));
    }

    public function update(Request $request)
    {
        $id = $request['id'];
        
        $this->validate($request, [
            'customer_id'     => 'required',
            'title'           => 'required',
            'email'           => 'nullable|email',
            'question.*'      => 'required',
            'field_type_id.*' => 'required',
            'field_values.*'  => 'required_if:field_type_id,2,3,4',
            'sort_order.*'    => 'digits_between:1,7'
        ]);
       
        if($id == '')
        {
            $request['email_subject']  = $request['title'];
            $request['email_template'] = '<b>Dear [[name]],</b><br><br>Your form has a new entry.<br><br> Here are the results.<br><br> [[All Answers]]<br><br><br>Regards<br>HQForms';
        }
        
        $request['user_id'] = userId();
        $requestData = $request->all();
        
        if($id == '')
        {
            $customForm = CustomForm::create($requestData);
        }else
        {
            $customForm = CustomForm::findOrFail($id);
            $customForm->update($requestData);
        }
        
        $this->maintainCustomFields($request, $customForm);
        
        Session::flash('flash_message', 'Form updated!');

        return response()->json(['response' => 'success']);
    }
    
    public function destroy($id)
    {
        $customForm = CustomForm::find($id);
        $customForm->delete();

        Session::flash('flash_message', 'Record Deleted!');
        return response()->json(['response' => 'success']);
    }
    
    public function deleteMultipleRecords(Request $request)
    {
        if($request['all'] == 1)
        {
            $result = CustomForm::delete();
        }else
        {
            $result = CustomForm::whereIn('id', explode(',', $request['form_ids']))->delete();
        }
        
        if($result > 0)
        {
            Session::flash('flash_message', 'Record(s) Deleted!');
            return response()->json(['response' => 'success']);
        }else
        {
            return response()->json(['message' => 'Record(s) not Deleted!']);
        }
    }
    
    private function maintainCustomFields($request, $customForm)
    {
        $where['form_id'] = $customForm->id;
        
        if(!empty($request['field_id']))
        {
            CustomField::whereNotIn('id', $request['field_id'])->where($where)->update(array('enabled' => 0));
        }  else {
            CustomField::where($where)->update(array('enabled' => 0));
        }    
        
        if(!empty($request['question']))
        {
            foreach($request['question'] as $key => $value):

                $default = $request['default'][$key];
                $fieldId = isset($request['field_id'][$key])?$request['field_id'][$key]:NULL;

                $data = array(
                            'question'      => $value,
                            'field_type_id' => $request['field_type_id'][$key],
                            'field_values'  => $request['field_values'][$key],
                            'required'      => isset($request['required'][$default])?1:0,
                            'sort_order'    => $request['sort_order'][$key],
                            'enabled'       => 1,
                        );
                if($fieldId == NULL)
                {
                    $grandData[] = new CustomField($data);
                }else{
                    $customField = CustomField::findOrFail($fieldId);
                    $customField->update($data);
                }
            endforeach;

            if(!empty($grandData))
            {
                $customForm->customField()->saveMany($grandData); 
            }
        }
    }

    public function editTemplate($id)
    {
        $customForm = CustomForm::findOrFail($id);

        return view('custom_forms.template', compact('customForm'));
    }
    
    public function updateTemplate(Request $request)
    {
        $this->validate($request, [
            'email_subject'     => 'required',
            'email_template'    => 'required',
            'custom_field_id.*' => 'required',
            'conditions.*'      => 'required',
            'emails.*'          => 'required',
        ]);
        
        $id = $request['id'];
        
        $requestData = $request->only('email_subject', 'email_template');
        
        $customForm = CustomForm::findOrFail($id);
        $customForm->update($requestData);
        
        $this->maintainNotifications($request, $customForm);

        Session::flash('flash_message', 'Email Template updated!');

        return response()->json(['response' => 'success']);
    }
    
    private function maintainNotifications($request, $customForm)
    {
        $where['form_id'] = $customForm->id;
        
        if(!empty($request['notification_id']))
        {
            EmailNotification::whereNotIn('id', $request['notification_id'])->where($where)->delete();
        }  else {
            EmailNotification::where($where)->delete();
        }    
        
        if(!empty($request['custom_field_id']))
        {
            foreach($request['custom_field_id'] as $key => $value):

                $fieldId = isset($request['notification_id'][$key])?$request['notification_id'][$key]:NULL;

                $data = array(
                            'custom_field_id' => $value,
                            'conditions'      => $request['conditions'][$key],
                            'values'          => $request['values'][$key],
                            'emails'          => $request['email'][$key],
                            'user_id'         => userId()
                        );
                if($fieldId == NULL)
                {
                    $grandData[] = new EmailNotification($data);
                }else{
                    $customField = EmailNotification::findOrFail($fieldId);
                    $customField->update($data);
                }
            endforeach;

            if(!empty($grandData))
            {
                $customForm->emailNotification()->saveMany($grandData); 
            }
        }
    }
    
    public function formFill($id)
    {
        $customForm = CustomForm::findOrFail($id);
        
        return view('custom_forms.form', compact('customForm'));
    }

    public function storeFormFill(Request $request)
    {
        if(!empty($request['field_values']))
        {
            $formId = $request['form_id'];
            
            $request['user_id'] = userId();
            $requestData = $request->all();
            
            $customFormData = CustomFormData::create($requestData);
/*            
            $fieldName  = $request['field_name'];
            $responseId = CustomFormData::max('response_id') + 1;
        
            $grantData = array();
            
            foreach($fieldName as $key => $value):
                
                if($value != '')
                {
                    $data['field_value']     = ($request['field_type'][$key] == 4)?join(',', $value):$value;
                    $data['custom_field_id'] = $key;
                    $data['form_id']         = $formId;
                    $data['response_id']     = $responseId;
                    $data['user_id']         = userId();
                    $data['created_at']      = date('Y-m-d H:i:s');
                    $data['updated_at']      = date('Y-m-d H:i:s');

                    $grantData[] = $data;
                }
            endforeach;
            
            if(!empty($grantData))
            {
                CustomFormData::insert($grantData); 
            }*/
            
            $send = $this->sendEmail($formId, $customFormData);
            
            if($send)
            {
                Session::flash('flash_message', 'Form submitted!');
                return response()->json(['response' => 'success']);
            }  else {
                return response()->json(['message' => trans('Form submitted but Email Not Send!')]);
            }
        }
    }
 
    private function sendEmail($formId, $customFormData)
    {
        try
        {
            $customForm = CustomForm::findOrFail($formId);
            //$customFormData = CustomFormData::where(array('form_id' => $formId, 'id' => $responseId))->firstOrFail();
            
            $templateData = $this->templateData($customForm, $customFormData, $customForm->email_subject, $customForm->email_template);
            $subject = $templateData['subject'];
            $emails = $this->emails($formId, $customFormData->id, $customForm);
            
            $data = array('template' => $templateData['template']);

            if($emails != '')
            {
                \Mail::send('emails.index', $data, function($message) use($customForm, $subject, $emails)
                {
                    $message->from(userEmail(), userName());
                    
                    $message->to(explode(',', $emails))->subject($subject);
                });
            }
            return true;
        }
        catch( Swift_TransportException $e)
        {
            Log::error('Cannot send the email: '.$e->getMessage());
            return false;
        }
        catch( Swift_RfcComplianceException $e)
        {
            Log::error('Cannot send the email: '.$e->getMessage());
            return false;
        }
    }
    
    private function emails($formId, $responseId, $customForm)
    {
        $emails = array();
        
        if($customForm->emails != '')
        {
            $emails[] = $customForm->emails;
        }
        
        if($customForm->enable_email == TRUE && $customForm->email != '')
        {
            $emails[] = $customForm->email;
        }

        foreach($customForm->emailNotification as $row):
            
            $conditions = $row->conditions;
            $fieldId    = $row->custom_field_id;
            $values     = $row->values;
            
            if($conditions == 'equals' || $conditions == 'equal_to'):
                $where = "field_values->>'{$fieldId}' = '".$values."'"; 
            elseif($conditions == 'contains'):
                $where = "field_values->>'{$fieldId}' ilike '%".$values."%'"; 
            elseif($conditions == 'does_not_contain'):
                $where = "field_values->>'{$fieldId}' NOT ilike '%".$values."%'";
            elseif($conditions == 'is_empty'):
                $where = "field_values->>'{$fieldId}' IS NULL"; 
            elseif($conditions == 'is_not_empty'):
                $where = "field_values->>'{$fieldId}' IS NOT NULL"; 
            elseif($conditions == 'not_equal_to'):
                $where = "field_values->>'{$fieldId}' != '".$values."'"; 
            elseif($conditions == 'greater_than'):
                $where = "CAST(field_values->>'{$fieldId}' AS INTEGER) > ".$values;
            elseif($conditions == 'less_than'):
                $where = "CAST(field_values->>'{$fieldId}' AS INTEGER) < ".$values; 
            elseif($conditions == 'regex_matches'):
                $where = "field_values->>'{$fieldId}' ~* '".$values."'"; 
            elseif($conditions == 'regex_not_matches'):
                $where = "field_values->>'{$fieldId}' !~* '".$values."'"; 
            endif;
            
            $count = CustomFormData::where(array('form_id' => $formId, 'id' => $responseId))->whereRaw($where)->count();
            
            if($count > 0)
            {
                $emails[] = $row->emails;
            }   
            
        endforeach;
              
        return join(',', $emails);
    }

    private function templateData($customForm, $customFormData, $subject, $template)
    {
        $data = '';
 
        $templateReplace['[[name]]'] = getCustomerName($customForm);
        
        if($customFormData->count() > 0): 
            foreach ($customForm->customField as $row):
                    
                    $fieldValue = $customFormData->field_values[$row->id];
                    $value = (is_array($fieldValue))?join(', ', $fieldValue):$fieldValue;
                    
                    $data .= '<tr><td width="30%" style="padding-bottom:30px; border-bottom:1px solid #eeeeee;"><b>'.$row->question.'</b></td><td width="70%" style="padding-bottom:30px; border-bottom:1px solid #eeeeee;">'.$value.'</td>';
                    
                    $subjectReplace['[['.$row->question.']]']  = $value;
                    $templateReplace['[['.$row->question.']]'] = '<b>'.$row->question.': </b>'.$value;
            endforeach;
            
            $allAnswers = '<table border="0" width="100%" cellpadding="10" cellspacing="0" style="background:#fbfbfb; border:1px solid #eeeeee; font-size:12px">'.$data.'</table>';
            
            $templateReplace['[[All Answers]]'] = $allAnswers;
            
        endif;
        
        $sub  = $this->strReplaceAssoc($subjectReplace, $subject);
        $temp = $this->strReplaceAssoc($templateReplace, $template);
        
        return array('subject' => $sub, 'template' => $temp);
    }
    
    private function strReplaceAssoc(array $replace, $subject) 
    {
        return str_replace(array_keys($replace), array_values($replace), $subject);   
    }
    
    public function duplicateForm(Request $request)
    {
        CustomForm::duplicateForm($request['id'], $request['title'], userId());
        
        Session::flash('flash_message', 'Form duplicated!');

        return response()->json(['response' => 'success']);
    }
    
    public function formResponses(Request $request)
    {
        $formId        = $request['form_id'];
        $customFieldId = $request['custom_field_id'];
        $fieldValue    = $request['field_value'];
        $customForm    = CustomForm::findOrFail($formId);
        
        if($customFieldId == '')
        {
            $customFieldId = array();
        }
        
//        $previous       = CustomFormData::where('form_id', $formId)->where('response_id', '<', $responseId)->max('response_id');
//        $next           = CustomFormData::where('form_id', $formId)->where('response_id', '>', $responseId)->min('response_id');

        return view('custom_forms.form_responses', compact('formId', 'customFieldId', 'fieldValue', 'customForm'));
    }
    
    public function formResponsesData(Request $request)
    {
        $formId        = $request['form_id'];
        $customFieldId = $request['custom_field_id'];
        $fieldValue    = $request['field_value'];
        
        $customFormData = CustomFormData::where(function($query) use ($formId, $customFieldId, $fieldValue) 
                            {
                                $query->where('form_id', $formId);
                                
                                if($customFieldId != '' && $fieldValue != '')
                                {
                                    foreach ($customFieldId as $key => $fieldId) 
                                    {
                                        if($key == 0)
                                        {
                                            $query->whereRaw("field_values->>'{$fieldId}' ilike '%".$fieldValue."%'");
                                        }else
                                        {
                                            $query->orWhereRaw("field_values->>'{$fieldId}' ilike '%".$fieldValue."%'");
                                        }
                                    }
                                }
                            });
        
        return Datatables::eloquent($customFormData)->make(true);
    }
    
    public function exportFormResponses($formId)
    {
        $fileName = date('dmY-His');
        
        Excel::create($fileName, function($excel) use($fileName, $formId) 
        {
            $excel->sheet($fileName, function($sheet) use($formId)
            {
                $customForm = CustomForm::findOrFail($formId);
                $customFormData = CustomFormData::where('form_id', $formId)->get();
                
                $sheet->loadView('custom_forms.export_form_responses', compact('customForm', 'customFormData'));
            });
        })->export('xlsx');
    }
    
    public function createUser()
    {
        $user = new User;
        
        return view('laravel-authentication-acl::admin.user.create', compact('user'));
    }

    public function storeUser(Request $request)
    {
        $this->validate($request, [
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required|min:6',
            'first_name' => 'required|max:50',
            'last_name'  => 'required|max:50',
            'phone'      => 'required|max:20',
            'vat'        => 'max:20',
            'state'      => 'max:20',
            'city'       => 'max:50',
            'country'    => 'max:50',
            'zip'        => 'max:20',
            'address'    => 'max:100',
        ]);
        
        $email = $request['email'];
        $password = $request['password'];

        $userRepository = \App::make('user_repository');
        $data = array(
            "email"     => $email,
            "password"  => $password,
            "activated" => 1,
            "banned"    => 0
        );

        $user = $userRepository->create($data);
        $userId = $user->id;
        
        $request['user_id'] = $userId;
        $requestData = $request->all();

        UserProfile::create($requestData);
            
        $userRepository->addGroup($userId, $request['group_id']);

        $mailer = \App::make('jmailer');

        $viewFile = "laravel-authentication-acl::admin.mail.registration-confirmed-client";

        // send email to client
        $mailer->sendTo($email, [
                "email"      => $email,
                "password"   => $password,
                "first_name" => $request['first_name'],
            ],
            \Config::get('acl_messages.email.user_registration_request_subject'), $viewFile);
        
        Session::flash('flash_message', 'Customer addated!');

        return response()->json(['response' => 'success']);
    }
    
    public function addCustomField($i, $no)
    {
        return view('custom_forms.custom_fields', compact('i', 'no'));
    }
    
    public function addNotification($formId, $i)
    {
        return view('custom_forms.notification', compact('formId', 'i'));
    }
}
