<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CustomField extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_fields';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'form_id', 'field_type_id', 'field_values', 'required', 'sort_order', 'enabled'];

    public function fieldType()
    {
        return $this->belongsTo('App\CustomFieldType');
    }
}
