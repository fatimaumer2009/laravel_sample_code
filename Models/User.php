<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use LaravelAcl\Authentication\Models\User as SentryUser;
Use DB;

class User extends SentryUser
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
/*    protected $fillable = [
        'name', 'email', 'password',
    ];
*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
/*    protected $hidden = [
        'password', 'remember_token',
    ];
*/    
    public function getPersistCode()
    {
        if (!$this->persist_code)
        {
            $this->persist_code = $this->getRandomString();

            // Our code got hashed
            $persistCode = $this->persist_code;

            $this->save();

            return $persistCode;            
        }
        return $this->persist_code;
    }
    
    public function scopeGetCustomers($query)
    {
        $users = User::select(DB::raw('users.id, user_profile.first_name, user_profile.last_name'))
                ->join('user_profile', function ($join) 
                {
                    $join->on('users.id', '=', 'user_profile.user_id');
                    
                })->join('users_groups', function ($join) 
                {
                    $join->on('users.id', '=', 'users_groups.user_id');
                    
                })->where('users_groups.group_id', 3)->get();

        return $users;
    }
/*    
    public function userProfile()
    {
        return $this->belongsTo('LaravelAcl\Authentication\Models\UserProfile', 'id', 'user_id');
    }*/
}
