<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CustomForm extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_forms';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'emails', 'email', 'enable_email', 'description', 'email_subject', 'email_template', 'customer_id', 'user_id'];
    
    public function scopeDuplicateForm($query, $formId, $title, $userId)
    {
        DB::beginTransaction();

        $insert = "INSERT INTO custom_forms (title, email_subject, emails, email_template, description, customer_id, user_id, created_at, updated_at)
                        SELECT '{$title}', email_subject, emails, email_template, description, customer_id, ".$userId.", '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."' FROM custom_forms WHERE id = {$formId}";
        
        DB::insert($insert);
        
        $result = DB::select("SELECT currval(pg_get_serial_sequence('custom_forms', 'id'))");
        
        $id = $result[0]->currval;
        
        $insert2 = "INSERT INTO custom_fields (question, form_id, field_type_id, field_values, required, sort_order, enabled, created_at, updated_at)
                        SELECT question, {$id}, field_type_id, field_values, required, sort_order, enabled, '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."' FROM custom_fields WHERE form_id = {$formId} AND enabled = TRUE";
        
        DB::insert($insert2);
        
        DB::commit();
    }
    
    public function customField()
    {
        return $this->hasMany('App\CustomField', 'form_id')->where('enabled', 1)->orderBy('sort_order', 'ASC');
    }
    
    public function emailNotification()
    {
        return $this->hasMany('App\EmailNotification', 'form_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }
    
    public function userProfile()
    {
        return $this->belongsTo('LaravelAcl\Authentication\Models\UserProfile', 'customer_id', 'user_id');
    }
}
