<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFormData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_form_data';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['form_id', 'field_values', 'user_id'];
    
    protected $casts = [
        'field_values' => 'collection',
    ];
     
    public function customField()
    {
        return $this->belongsTo('App\CustomField');
    }
}